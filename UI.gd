extends CanvasLayer

onready var left_score = $Left/Score
onready var left_name = $Left/Name
onready var right_score = $Right/Score
onready var right_name = $Right/Name


func update_score(p1, p2):
    left_score.text = str(p1)
    right_score.text = str(p2)


func set_names(p1_name, p2_name):
    left_name.text = p1_name
    right_name.text = p2_name
    
