extends Node2D
class_name Game
var p1_score = 0
var p2_score = 0

onready var Player1 = $Player
onready var Player2 = $Player2
onready var UI = $UI

signal score_update(p1, p2)

func _ready():
    emit_signal("score_update", p1_score, p2_score)

func _on_LeftScore_body_entered(body):
    p2_score += 1
    emit_signal("score_update", p1_score, p2_score)

func _on_RightScore_body_entered(body):
    p1_score += 1
    emit_signal("score_update", p1_score, p2_score)


func init_game(player1, player2):
    UI.set_names(player1.name, player2.name)
    set_master(1, player1.id)
    set_master(2, player2.id)
    #merge input map when only 1 player
    if player1.id != player2.id:
        for key in Player1.action_map.keys():
            for action in Player2.action_map[key]:
                Player1.action_map[key].append(action)
        Player2.action_map = Player1.action_map
        
    

func set_master(player_num, id):
    print("setting network master of player %s to %s" % [player_num, id])
    if player_num == 1:
        Player1.set_network_master(id)
    elif player_num == 2:
        Player2.set_network_master(id)        
    else:
        print("tried to set an invalid player master")

