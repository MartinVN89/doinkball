extends VBoxContainer

onready var player_list : ItemList = $GamePlayers/playerList
onready var start_button :Button = $StartButton

class Connected_players:
    var players = []
    
    func add(id, name="..connecting.."):
        players.append({'id': id, 'name': name})
        players.sort_custom(self, "sortFunc")


    func remove(id):
        for i in range(players.size()):
            if players[i].id == id:
                players.remove(i)
                return OK
        return FAILED


    func update(id, name):
        for i in range(players.size()):
            if players[i].id == id:
                players[i].name = name
                players.sort_custom(self, "sortFunc")
                return OK
        return FAILED


    func get(i):
        return players[i]


    func size():
        return players.size()

    func getId(id):
        for i in range(players.size()):
            if players[i].id == id:
                return players[i]
        return -1
    
    
    func has(id):
        for i in range(players.size()):
            if players[i].id == id:
                return true
        return false


    func sortFunc(a,b):
        return a.name < b.name
 
var connected_players = Connected_players.new()

func _on_player_connected(id):
    if not multiplayer.is_network_server() or id == 1:
        return
    print("lobby: player connected %s" %id)
    # Sending info to new player
    for i in connected_players.size():
        var this_player = connected_players.get(i)
        rpc_id(id, "_update_player", this_player.id, this_player)


func _on_player_disconnected(id):
    print("lobby: player disconnected")
    rpc("_remove_player", id)


func _on_register_player(id, player_info):
    # print("lobby: player register")
    rpc("_update_player", id, player_info)


sync func _update_player(id, player_info):
    var hasId = connected_players.has(id)
    if not hasId:
        connected_players.add(id, player_info.name)
    else:
        print(connected_players.players)
        connected_players.update(id, player_info.name)

    update_view()


func update_view():
    player_list.clear()
    for i in range(connected_players.size()):
        player_list.add_item(connected_players.get(i).name)
    
    start_button.disabled = not can_start_game()


sync func _remove_player(id):
    var res = connected_players.remove(id)
    if res != OK:
        print("Problem removing player from connected_players!")
    update_view()

func can_start_game():
    if not multiplayer.is_network_server():
        return false
    if connected_players.players.size() >= 2:
        return true

func _on_BackButton_pressed():
    #Disconnect any servers\clients and return to main menu
    multiplayer.set_network_peer(null)
    ws.disconnect_from_server()
    connected_players.players.clear()
    get_parent().hide_all()
    get_parent().main_menu_node.visible = true
    pass # Replace with function body.


func _on_StartButton_pressed():
    rpc("start_game")


sync func start_game():
    get_parent().visible = false
    var players = connected_players.players
    print("starting game")
    
    #No need to stay connected to the hub when starting the game. 
    # This also removes the game from the "open game list"
    ws.disconnect_from_server() 
    
    var game = load("res://Game.tscn").instance()
    get_parent().game_node.add_child(game)
    if players.size() < 2:
        players.append({'name': "Player2", 'id': 1})
    game.init_game(players[0], players[1])
    
    
func _on_GameLobby_visibility_changed():
    update_view()


func clear_players():
    connected_players.players.clear()

