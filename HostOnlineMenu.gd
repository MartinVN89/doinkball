extends VBoxContainer


#onready var ws_server = $ws_server
onready var host_button : Button = $HostButton
onready var username : LineEdit = $VBoxContainer/HBoxContainer/UserName
onready var game_name : LineEdit = $VBoxContainer/HBoxContainer2/GameName
onready var server_port : LineEdit = $VBoxContainer/HBoxContainer3/ServerPort
onready var status_messages : Label = $VBoxContainer/StatusMessages

signal host_server(port)

func _ready():
    server_port.text = str(Global.SERVER_PORT)

func _on_BackButton_pressed():
    visible = false
    host_button.disabled = true
    ws.disconnect_from_server()
    get_parent().main_menu_node.visible = true
    

func can_host():
    if not server_port.text.is_valid_integer():
        return false
    if username.text.length() == 0:
        return false
    if game_name.text.length() == 0:
        return false
    return true


func update_view():
    host_button.disabled = not can_host()

func _on_ServerPort_text_changed(new_text):
    update_view()


func _on_GameName_text_changed(new_text):
    update_view()


func _on_UserName_text_changed(new_text):
    update_view()


func _on_HostOnlineMenu_visibility_changed():
    update_view()


func _on_HostButton_pressed():
    var port = int(server_port.text)
    var game_name_txt = game_name.text
    var username_txt = username.text
    ws.connect("status_update", self, "_on_ws_server_status_update")
    #crate_game_host(gameName, gameID, maxPlayers, username, port):
    ws.crate_game_host(game_name_txt,Global.GAME_ID, Global.MAX_PLAYERS, username_txt, port)
    emit_signal("host_server", port)
    host_button.disabled = true

func _on_ws_server_status_update(msg):
    status_messages.text = msg