extends Node

var USE_ENET = true
const SERVER_PORT = 4501
const MAX_PLAYERS = 2
const GAME_ID = "DoinkBall"
var Network : NetworkClass
var Lobby

func _ready():
    if OS.get_name()=="HTML5":
        USE_ENET = false

func _log(msg):
    print(msg)