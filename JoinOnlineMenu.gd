extends VBoxContainer

#onready var ws_hub = $ws_client
onready var hub_games_list : ItemList = $HubGames/ItemList
onready var status_label : Label = $HubGames/status

onready var back_button : Button = $"BackButton"
onready var connect_button : Button = $HubGames/ConnectButton

signal join_game_server(ip, port)
signal name_change(name)
var game_list
var username


func _ready():
    ws.connect("status_update", self, "_on_ws_server_status_update")
    ws.connect("got_game_list", self, "process_game_list")

func _on_BackButton_pressed():
    visible = false
    connect_button.disabled = true
    hub_games_list.clear()
    status_label.text = ""
    ws.disconnect_from_server()
    get_parent().main_menu_node.visible = true


func _on_ConnectButton_pressed():
    if not hub_games_list.is_anything_selected():
        connect_button.disabled = true
        return
    var selected_items = hub_games_list.get_selected_items()
    var index = selected_items[0]
    _on_ItemList_item_activated(index)


func _on_ws_server_status_update(msg):
    status_label.text = msg

func _on_ws_server_got_game_list(_game_list):
    game_list = _game_list
    process_game_list(game_list)
    


func process_game_list(game_list):
    hub_games_list.clear()
    for game in game_list:
        if game.IP == ws.port_open.get_external_address():
            print("got a local game")
            game.IP = game.localIP
            if game.IP == ws.port_open.get_internal_address():
                print("got localhost game")
                game.IP = "127.0.0.1"
            game.local_game = true
        else:
            game.local_game = false
        # game.erase("local_ip")
        hub_games_list.add_item(game.gameName)

    _on_ws_server_status_update("Received %s open games from server" % game_list.size())

func _on_ItemList_item_activated(index):
    var game = ws.game_list[index]
    print(game)
    if not game.local_game:
        ws.port_open.do_port_forward(game.port)
    emit_signal("join_game_server", game.IP, game.port)
    _on_ws_server_status_update("Joining server..")


func _on_ItemList_item_selected(index):
    connect_button.disabled = false


func _on_refreshButton_pressed():
    ws.refresh_games()