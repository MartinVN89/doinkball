extends KinematicBody2D

const UP = Vector2(0, -1)

export var GRAVITY = 250
export var FALLING_GRAVITY = 500
export var WALK_FORCE = 1000
export var MAX_SPEED = 150
export var JUMP_FORCE = 250
export var FRICTION = 0.5
export(Texture) var SPRITE 

export var action_map = {
    'left': ['left'],
    'right': ['right'],
    'jump': ['jump1'],
    }

var velocity = Vector2()
var jumping = false

var start_pos = null

puppet var slave_position = Vector2()
puppet var slave_velocity = Vector2()

func _ready():
    if SPRITE:
        $Sprite.texture = SPRITE
    start_pos = position


func on_goal():
    position = start_pos
    
func input_helper(actions):
    for action in actions:
        if Input.is_action_pressed(action):
            return true
    return false


func input_helper_immediate(actions):
    for action in actions:
        if Input.is_action_just_pressed(action):
            return true
    return false

func _physics_process(delta):
    if multiplayer.has_network_peer() and not is_network_master():
        slave_physics_process(delta)
        return
    var walk_left = input_helper(action_map["left"])
    var walk_right = input_helper(action_map["right"])
    var jump = input_helper_immediate(action_map["jump"])
    var stop_jump = not input_helper(action_map["jump"])
#    var walk_left = Input.is_action_pressed(action_map["left"])
#    var walk_right = Input.is_action_pressed(action_map["right"])
#    var jump = Input.is_action_just_pressed(action_map["jump"])
#    var stop_jump = not Input.is_action_pressed(action_map["jump"])

    var force = Vector2()
    if is_falling():
        force = Vector2(0,FALLING_GRAVITY)
    else:
        force = Vector2(0,GRAVITY)
        
    if walk_left and velocity.x > -MAX_SPEED:
        force.x -= WALK_FORCE
    if walk_right and velocity.x < MAX_SPEED:
        force.x += WALK_FORCE
    if (walk_right and walk_left) or (not walk_right and not walk_left):
        velocity.x = lerp(velocity.x, 0, 1.0-FRICTION)
    if (stop_jump and jumping):
        velocity.y = lerp(velocity.y, 0, 1.0-FRICTION)


    velocity += force * delta
    velocity = move_and_slide(velocity, UP)


    if jumping and is_falling(): #Now faling
        jumping = false

    if jump and can_jump():
        apply_jump()
        jumping = true
    if multiplayer.has_network_peer():
        rset_unreliable("slave_position", position)
        rset("slave_velocity", velocity)  

func slave_physics_process(delta):
    #TODO state interpolation..
    position= slave_position
    velocity = slave_velocity
    velocity = move_and_slide(velocity, UP)

func is_falling():
    return velocity.y > 0

func can_jump():
    return is_on_floor()

func apply_jump():
    velocity.y = -JUMP_FORCE