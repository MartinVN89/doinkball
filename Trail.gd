extends Line2D

export (NodePath) var targetNode
export var trailLength = 128

var target
func _ready():
    set_as_toplevel(true)
    target = get_node(targetNode)

func _process(delta):
    add_point(target.position)
    while get_point_count() > trailLength:
        remove_point(0)


func clear_points():
    print("clear points")
    while get_point_count() > 0:
        remove_point(0)
    set_process(false)
    yield(get_tree(), "idle_frame")
    yield(get_tree(), "idle_frame")
    set_process(true)
        