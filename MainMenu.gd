extends VBoxContainer

onready var game_node = $"../GamePlaceholder"
onready var main_menu_node = $MainMenu
onready var open_games_node = $JoinOnlineMenu
onready var game_lobby = $GameLobby
onready var host_node = $HostOnlineMenu
onready var options_node = $OptionsMenu

func _ready():
    if OS.get_name()=="HTML5":
        $MainMenu/HostOnline.disabled = true
    hide_all()
    main_menu_node.visible = true


func hide_all():
    main_menu_node.visible = false
    open_games_node.visible = false
    game_lobby.visible = false
    host_node.visible = false
    options_node.visible = false

func _on_StartLocal_pressed():
    main_menu_node.visible = false
    self.visible = false
    var game : Game = load("res://Game.tscn").instance()
    game_node.add_child(game)
    game.init_game({'name': "Player 1", 'id': 1}, {'name': "Player 2", 'id': 1})
    print("start a local game")


func _on_JoinOnline_pressed():
    hide_all()
    open_games_node.visible = true
    ws.connect_to_server()


func _on_HostOnline_pressed():
    hide_all()
    host_node.visible = true
    ws.connect_to_server()


func _on_Options_pressed():
    #hide main menu and show options
    pass # Replace with function body.



func _on_Main_server_connected():
    hide_all()
    game_lobby.visible = true
    

func is_game_running():
    return game_node.get_child_count() > 0


func stop_game():
    game_node.get_child(0).queue_free()
    multiplayer.set_network_peer(null)
    ws.disconnect_from_server()


func _on_Main_server_disconnected():
    stop_game_if_running()


func _on_Main_player_disconnected(id):
    stop_game_if_running()


func stop_game_if_running():
    if is_game_running():
        stop_game()
        game_lobby.clear_players()
        visible = true
        hide_all()
        main_menu_node.visible = true