extends Node
class_name NetworkClass


#var player_info = {}
var my_info = {'name': "my_name"}
signal player_connected(id)
signal player_disconnected(id)
signal server_connected()
signal server_disconnected()
signal register_player(id, player_info)


func _ready():
    Global.Network = self
    set_process(false)
    multiplayer.connect("connection_failed", self, "_connection_failed")
    multiplayer.connect("connected_to_server", self, "_connected")
    multiplayer.connect("network_peer_disconnected", self, "_player_disconnected")
    multiplayer.connect("network_peer_connected", self, "_player_connected")
    multiplayer.connect("server_disconnected", self, "_server_disconnected")

func _exit_tree():
    Global.Network = null
    multiplayer.disconnect("connection_failed", self, "_connection_failed")
    multiplayer.disconnect("connected_to_server", self, "_connected")
    multiplayer.disconnect("network_peer_disconnected", self, "_player_disconnected")
    multiplayer.disconnect("network_peer_connected", self, "_player_connected")
    multiplayer.disconnect("server_disconnected", self, "_server_disconnected")

func _connection_failed():
    Global._log("Could not connect to server")
    multiplayer.set_network_peer(null)
    set_process(false)


func _server_disconnected():
    Global._log("Server closed connection")
    multiplayer.set_network_peer(null)
    set_process(false)
    emit_signal("server_disconnected")


func _connected():
    Global._log("connected to server")
    var selfPeerID = multiplayer.get_network_unique_id()
    rpc("_rpc_player_connected", selfPeerID)
    rpc("_rpc_register_player", selfPeerID, my_info)
    emit_signal("server_connected")
#    _rpc_register_player(selfPeerID, my_info)

func _close_network():
    multiplayer.set_network_peer(null)


func _player_disconnected(id):
    Global._log("Peer disconnected from server: %s" %id)
    rpc("_rpc_player_disconnected", id)


func _player_connected(id):
    Global._log("Peer connected to server: %s" %id)


func start_server(port):
    print("starting server at port: %s" % port)
    var peer = null
    var error = null
    if Global.USE_ENET:
        peer = NetworkedMultiplayerENet.new()
        error = peer.create_server(port, Global.MAX_PLAYERS)
    else:
        peer = WebSocketServer.new()
        error = peer.listen(port, PoolStringArray(["demo"]), true)
        set_process(true)
    multiplayer.set_network_peer(peer)
    
    if error != OK:
        print("Problem starting server: %s" % error)
    else:
        emit_signal("server_connected") 

func start_client(host, port):
    print("starting client and connecting to: %s:%s" % [host, port])
    var peer = null
    if Global.USE_ENET:
        peer = NetworkedMultiplayerENet.new()
        peer.create_client(host, port)
    else:
        peer = WebSocketClient.new()
        peer.connect_to_url("ws://%s:%s/" % [host, port], PoolStringArray(["demo"]), true)
        set_process(true)
    multiplayer.set_network_peer(peer)

var network_peer = null
func _process(delta):
    #For some reason you have to poll when using websocketclient\server.
    # but when using Enet doing custom_multiplayer.pool in the parent is enough..
    if network_peer == null and multiplayer.has_network_peer():
        network_peer = multiplayer.get_network_peer()
    if network_peer:
        network_peer.poll()

#func _on_LobbyUI_join_server(ip, port):
#    start_client(ip, Global.SERVER_PORT) 


#func _on_LobbyUI_host_server(port):
#    start_server(Global.SERVER_PORT)
#    _rpc_player_connected(1)
#    _rpc_register_player(1, my_info)

#
#func _on_LobbyUI_local_player(_player_info):
#    my_info.name = _player_info.name

sync func _rpc_player_connected(id):
    emit_signal("player_connected", id)

sync func _rpc_register_player(id, my_info):
    emit_signal("register_player", id, my_info)

sync func _rpc_player_disconnected(id):
    emit_signal("player_disconnected", id)

func _on_LobbyUI_leave_server():
    multiplayer.set_network_peer(null)


onready var game_placeholder = $GamePlaceholder

func _on_LobbyUI_start_game(players):
    print("starting game")
    var game = load("res://Game.tscn").instance()
    game_placeholder.add_child(game)
    if players.size() < 2:
        players.append({'name': "Player2", 'id': 1})
    game.init_game(players[0], players[1])


func _on_JoinOnlineMenu_join_game_server(ip, port):
    start_client(ip, port)


func _on_name_change(name):
    my_info.name = name


func _on_HostOnlineMenu_host_server(port):
    start_server(port)
    _rpc_player_connected(1)
    _rpc_register_player(1, my_info)