extends RigidBody2D

const MAX_SPEED = 450

onready var start_pos = $"../BallStartPos"
onready var hit_sound = $HitSound
onready var point_sound = $PointSound
onready var trail = $Trail

var physics_steps = 0

var reset = true
var time_to_sync = true

puppet var sync_data = null


func _integrate_forces(state):
    physics_steps += 1
    if reset:
        reset_ball(state)
    if multiplayer.has_network_peer() and is_network_master() and time_to_sync:
        _sync_ball(state)    
    if multiplayer.has_network_peer() and not is_network_master() and sync_data:
        state.transform = sync_data['transform']
        state.linear_velocity = sync_data['linear_velocity']
        state.angular_velocity = sync_data['angular_velocity']
        
#        var master_steps = sync_data['step']
#        while master_steps < physics_steps:
#            state.integrate_forces()
#            master_steps += 1
        
        sync_data = null
        
    if state.linear_velocity.length() > MAX_SPEED:
        state.linear_velocity = state.linear_velocity.normalized() * MAX_SPEED

func reset_ball(state : Physics2DDirectBodyState):
#    print("reset ball")
    if start_pos:
        state.transform = start_pos.transform
    if trail:
        trail.clear_points()
    state.linear_velocity = Vector2()
    state.angular_velocity = 0
    apply_central_impulse(Vector2(0, -100))
    add_torque(250 * rand_range(-1, 1))
    reset = false
    time_to_sync = true
    sync_data = null


func _sync_ball(state):
    var t : Transform2D = state.transform
    rpc("sync_ball", state.transform, state.linear_velocity, state.angular_velocity, physics_steps)
    time_to_sync = false


remote func sync_ball(_transform, _velocity, _angular, _step):
#    print("sync ball")
    sync_data = {
        'transform' : _transform,
        'linear_velocity': _velocity,
        'angular_velocity': _angular,
        'step': _step
         }
    

func _on_LeftScore_body_entered(body):
    print("right point")
    reset = true
    point_sound.play()


func _on_RightScore_body_entered(body):
    print("left point")
    reset = true
    point_sound.play()


func _on_Ball_body_entered(body):
    time_to_sync = true
    hit_sound.play()


func _on_SyncTimer_timeout():
    time_to_sync = true
